import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from './users/users.module';
import { AddressesModule } from './addresses/addresses.module';
import { PhoneNumbersModule } from './phone-numbers/phone-numbers.module';
import { User } from './users/entities/user.entity';
import { Address } from './addresses/entities/address.entity';
import { PhoneNumber } from './phone-numbers/entities/phone-number.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'contact.db',
      entities: [User, Address, PhoneNumber],
      synchronize: true, //development only
    }),
    UsersModule,
    AddressesModule,
    PhoneNumbersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
