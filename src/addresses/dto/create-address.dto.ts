import { IsString } from 'class-validator';

export class CreateAddressDto {
  @IsString()
  public readonly content: string;

  @IsString()
  public readonly type: string;
}
