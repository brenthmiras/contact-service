import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

describe('UsersController', () => {
  let controller: UsersController;

  const findAllMock = jest.fn().mockResolvedValue(['user1', 'user2', 'user3']);
  const createMock = jest.fn().mockResolvedValue('createdUser');

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: {
            findAll: findAllMock,
            create: createMock,
          },
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('#findAll', () => {
    it('should handle get users query', async () => {
      const result = await controller.findAll();

      expect(findAllMock).toBeCalledTimes(1);

      expect(result).toEqual(['user1', 'user2', 'user3']);
    });
  });

  describe('#create', () => {
    it('should handle get users query', async () => {
      const result = await controller.create({
        email: 'test@example.com',
        addresses: [
          {
            type: 'billing',
            content: 'St 1 Blk 27 Gotham City',
          },
        ],
        phoneNumbers: [
          {
            type: 'home',
            content: '639123456789',
          },
        ],
      });

      expect(createMock).toBeCalledTimes(1);

      expect(result).toEqual('createdUser');
    });
  });
});
