import { CreateAddressDto } from '../../addresses/dto/create-address.dto';
import { ValidateNested, IsEmail } from 'class-validator';
import { Type } from 'class-transformer';
import { CreatePhoneNumberDto } from 'src/phone-numbers/dto/create-phone-number.dto';

export class CreateUserDto {
  @IsEmail()
  public readonly email: string;

  @Type(() => Array<CreateAddressDto>)
  @ValidateNested()
  readonly addresses: Array<CreateAddressDto>;

  @Type(() => Array<CreatePhoneNumberDto>)
  @ValidateNested()
  readonly phoneNumbers: Array<CreatePhoneNumberDto>;
}
