import { PhoneNumber } from 'src/phone-numbers/entities/phone-number.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Address } from '../../addresses/entities/address.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  email: string;

  @OneToMany((type) => Address, (address) => address.user)
  addresses: Address[];

  @OneToMany((type) => PhoneNumber, (phoneNumber) => phoneNumber.user)
  phoneNumbers: PhoneNumber[];
}
